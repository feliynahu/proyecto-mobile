using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorRandom : MonoBehaviour
{
    [SerializeField] Image Circulo;
    [SerializeField] Image Cuadrado;
    [SerializeField] Image Diamante;
    [SerializeField] List<Color> colores;

    void Start()
    {
    }

    void Update()
    {
        
    }

    public void ElegirColorRandom()
    {
        Circulo.color = colores[Random.Range(0, colores.Count)];
        Cuadrado.color = colores[Random.Range(0, colores.Count)];
        Diamante.color = colores[Random.Range(0, colores.Count)];
    }
    
}
