using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparicionObjetos : MonoBehaviour
{
    [SerializeField] GameObject CreadorDePisos;
    public GameObject[] Pisos;
    [SerializeField] GameObject[] TiposDeObjetos;
    [SerializeField] Material materialpisos;
    [SerializeField]List<GameObject> objetosTemporales;
    void Start()
    {
        Pisos = GameObject.FindGameObjectsWithTag("Piso");
        objetosTemporales = new List<GameObject>();
    }

    void Update()
    {

    }

    public void CrearObjetos()
    {

        for (int i = 0; i < Pisos.Length; i++)
        {
            GameObject rnd = TiposDeObjetos[Random.Range(0, TiposDeObjetos.Length)];
            objetosTemporales.Add(Instantiate(rnd, Pisos[i].transform.position + Vector3.up, rnd.transform.rotation));

        }
    }

    public void DestruirObjetos()
    {
        if (objetosTemporales.Count > 0)
        {
            foreach (GameObject objetos in objetosTemporales)
            {
                Destroy(objetos);
                objetosTemporales = new List<GameObject>();
            }
        }
    }

    public void CrearPisos()
    {
        foreach (GameObject piso in Pisos)
        {
            piso.GetComponent<Renderer>().material = materialpisos;
            piso.GetComponent<CambioDeColor>().Destruir = true;
            piso.SetActive(true);
        }
    }

    public void DestruirPisos()
    {
        GetComponent<ComprobacionColores>().ComprobarColores();
        foreach (GameObject piso in Pisos)
        {
            if (piso.GetComponent<CambioDeColor>().Destruir)
                piso.SetActive(false);
        }
    }
}
