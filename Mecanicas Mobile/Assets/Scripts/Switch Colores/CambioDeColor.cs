using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDeColor : MonoBehaviour
{
    [SerializeField] Material [] materiales;
    [SerializeField]public GameObject objetoColocado;
    public bool Destruir;
    void Start()
    {
        Destruir = true;
    }

    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (SelectorColores.Amarillo)
        {
            GetComponent<Renderer>().material = materiales[0];
        }
        else if (SelectorColores.Rojo)
        {
            GetComponent<Renderer>().material = materiales[1];
        }
        else if (SelectorColores.Azul)
        {
            GetComponent<Renderer>().material = materiales[2];
        }
    }

    public void NoDestruir()
    {
        this.Destruir = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        objetoColocado = collision.gameObject;
        //if(collision.gameObject.name == "Cuadrado")
        //{
            
        //}
        //else if (collision.gameObject.name == "Circulo")
        //{

        //}
        //else if (collision.gameObject.name == "Diamante")
        //{

        //}
    }

}
