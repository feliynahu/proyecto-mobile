using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComprobacionColores : MonoBehaviour
{
    [SerializeField] Image imagenCuadrado;
    [SerializeField] Image imagenCirculo;
    [SerializeField] Image imagenDiamante;
    [SerializeField] List<Renderer> colores;
    [SerializeField]GameObject[]Pisos;
    void Start()
    {
        foreach (GameObject pisos in Pisos)
        {
            colores.Add(pisos.GetComponent<Renderer>());
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)) ComprobarColores();
    }

    public void ComprobarColores()
    {
        foreach (GameObject pisos in Pisos)
        {
            if (pisos.GetComponent<CambioDeColor>().objetoColocado.gameObject.name.Substring(0,8) == "Cuadrado")
            {

                if(ColorUtility.ToHtmlStringRGBA(imagenCuadrado.color) == ColorUtility.ToHtmlStringRGBA(pisos.GetComponent<Renderer>().material.color))
                {
                    print(pisos + "es igual al cuadrado");
                    pisos.GetComponent<CambioDeColor>().NoDestruir();
                }
            }
            if (pisos.GetComponent<CambioDeColor>().objetoColocado.gameObject.name.Substring(0, 7) == "Circulo")
            {
                if (ColorUtility.ToHtmlStringRGBA(imagenCirculo.color) == ColorUtility.ToHtmlStringRGBA(pisos.GetComponent<Renderer>().material.color))
                {
                    print(pisos + "es igual al circulo ");
                    pisos.GetComponent<CambioDeColor>().NoDestruir();
                }
            }
            if (pisos.GetComponent<CambioDeColor>().objetoColocado.gameObject.name.Substring(0, 8) == "Diamante")
            {
                if (ColorUtility.ToHtmlStringRGBA(imagenDiamante.color) == ColorUtility.ToHtmlStringRGBA(pisos.GetComponent<Renderer>().material.color))
                {
                    print(pisos + "es igual al diamante");
                    pisos.GetComponent<CambioDeColor>().NoDestruir();
                }
            }
        }
    }
}
