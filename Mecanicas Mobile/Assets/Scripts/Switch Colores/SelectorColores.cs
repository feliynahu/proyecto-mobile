using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorColores : MonoBehaviour
{
    public static bool Amarillo;
    public static bool Rojo;
    public static bool Azul;

    [SerializeField] GameObject PanelRojo;
    [SerializeField] GameObject PanelAmarillo;
    [SerializeField] GameObject PanelAzul;
    void Start()
    {

    }

    void Update()
    {

    }

    public void SeleccionarAmarillo()
    {
        Amarillo = true;
        Azul = false;
        Rojo = false;
        PanelAmarillo.SetActive(false);
        PanelAzul.SetActive(true);
        PanelRojo.SetActive(true);
    }
    public void SeleccionarAzul()
    {
        Amarillo = false;
        Azul = true;
        Rojo = false;
        PanelAmarillo.SetActive(true);
        PanelAzul.SetActive(false);
        PanelRojo.SetActive(true);
    }
    public void SeleccionarRojo()
    {
        Amarillo = false;
        Azul = false;
        Rojo = true;
        PanelAmarillo.SetActive(true);
        PanelAzul.SetActive(true);
        PanelRojo.SetActive(false);
    }
}
