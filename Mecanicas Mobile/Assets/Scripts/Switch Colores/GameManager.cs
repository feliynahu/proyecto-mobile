using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField]private float FinalTime;
    private float TiempoNecesitado=16;
    [SerializeField] private GameObject timer;
    public static bool GameOver;
    [SerializeField] private GameObject CanvasGameOver;
    private bool Boton;

    private void Start()
    {
        GameOver = false;
        Boton = false;
    }
    void Update()
    {
        if (GameOver)
        {
            CanvasGameOver.SetActive(true);
        }
        Inicio();
    }

    public void IniciarModoDeJuego1()
    {
        StartCoroutine(Cronometro());
    }
    public void Reiniciar()
    {
        SceneManager.LoadScene(4);
    }
    public void BotonTerminar()
    {
        Boton = true;
    }
    public void Terminar()
    {
        if (Boton)
        {
            FinalTime = 1;
        }
    }
    public void Inicio()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);
    }
    IEnumerator Cronometro()
    {
        TiempoNecesitado--;
        FinalTime = TiempoNecesitado;
        Boton = false;
        GetComponent<AparicionObjetos>().DestruirObjetos();
        GetComponent<AparicionObjetos>().CrearPisos();
        GetComponent<AparicionObjetos>().CrearObjetos();
        GetComponent<ColorRandom>().ElegirColorRandom();
        timer.GetComponent<Button>().interactable = false;
        while (FinalTime > 2)
        {
            FinalTime -= Time.deltaTime;
            timer.GetComponent<TextMeshProUGUI>().text = Mathf.RoundToInt(FinalTime - 2).ToString();
            Terminar();
            yield return null;
        }
        GetComponent<AparicionObjetos>().DestruirPisos();
        while (FinalTime > 0)
        {
            FinalTime -= Time.deltaTime;
            yield return null;
        }
        timer.GetComponent<TextMeshProUGUI>().text = "PLAY";
        timer.GetComponent<Button>().interactable = true;
    }
}
