using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnearEnemigos : MonoBehaviour
{
    public float respawningTimer;
    [SerializeField] private float yMax = 0;
    [SerializeField] private float yMin = 0;
    [SerializeField] private GameObject obstaculo;
    void Start()
    {
        
    }

    void Update()
    {
        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
               Instantiate(obstaculo, new Vector3(transform.position.x, UnityEngine.Random.Range(yMin, yMax), transform.position.z) , obstaculo.transform.rotation);
                respawningTimer = UnityEngine.Random.Range(3, 5);
            
        }
    }
}
