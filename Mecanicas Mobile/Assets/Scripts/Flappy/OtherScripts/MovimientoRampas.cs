using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoRampas : MonoBehaviour
{
    [SerializeField] private float velocidad;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        transform.position += Vector3.left * Time.deltaTime * velocidad;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fin"))
        {
            Destroy(this.gameObject);
        }
    }
}
