using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject prefab;

    private Queue<GameObject> pool;

    void Awake()
    {
        pool = new Queue<GameObject>();
        GrowPool();
    }

    public GameObject GetFromPool()
    {
        var nextObj = pool.Dequeue();
        nextObj.SetActive(false);
        return nextObj;
    }

    public void ReturnToPool (GameObject obj)
    {
        obj.SetActive(false);
        pool.Enqueue(obj);
    }

    private void GrowPool()
    {
        for (int i = 0; i < 10; i++)
        {
            var newObj = Instantiate(prefab);
            newObj.SetActive(false);
            pool.Enqueue(newObj);
        }
    }
}

