using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(Rigidbody))]

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float force;
    [SerializeField] private float gravity;
    [SerializeField] private float maxHeightThreshold;
    [SerializeField] private ForceMode forceMode;
    private Rigidbody playerRB;
    [SerializeField] GameObject gameOver;
    [SerializeField] TextMeshProUGUI puntajeText;
    private int puntaje = 0;

    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown(KeyCode.Space) && transform.position.y <= maxHeightThreshold)
        {
            playerRB.AddForce(Vector3.up * force, forceMode);
        }
        playerRB.AddForce(Vector3.down * gravity, ForceMode.Acceleration);
        puntajeText.text = "POINTS: " + puntaje.ToString(); 
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameOver.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        puntaje++;
    }

    public void Reiniciar()
    {
        SceneManager.LoadScene(5);
    }
}
