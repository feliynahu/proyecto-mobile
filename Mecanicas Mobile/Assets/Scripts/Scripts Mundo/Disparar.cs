using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    [SerializeField] private GameObject PrefabBala;
    [SerializeField] private int velocidad;
    void Update()
    {
        DispararProyectil();
    }

    private void DispararProyectil()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject disparo = Instantiate(PrefabBala, transform.position + transform.right, transform.rotation);
            disparo.GetComponent<Rigidbody>().AddForce(transform.right * velocidad, ForceMode.Impulse);
        }

    }
}
