using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBala : MonoBehaviour
{

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Mundo"))
        {
            transform.SetParent(collision.transform);
        }
        else if (collision.gameObject.CompareTag("Bala"))
        {
            GM_World.GameOver = true;
        }
    }
}
