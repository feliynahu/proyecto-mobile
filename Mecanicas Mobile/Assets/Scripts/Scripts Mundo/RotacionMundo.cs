using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionMundo : MonoBehaviour
{
    [SerializeField] int velocidadRotacion;
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, velocidadRotacion) * Time.deltaTime);
    }
}
