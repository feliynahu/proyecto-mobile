using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GM_World : MonoBehaviour
{
    public static bool GameOver;
    [SerializeField] GameObject pantallaGameOver;
    [SerializeField]  TMP_Text ContadorText;
    [SerializeField] GameObject Mundo;
    [SerializeField] int contador;
    void Start()
    {
        GameOver = false;
       
    }

    void Update()
    {
        ContadorDePuntos();
        if (GameOver)
        {
            Perder();
        }
        Inicio();
    }
    private void Perder()
    {
        //Time.timeScale = 0;
        pantallaGameOver.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    private void ContadorDePuntos()
    {
        contador = Mundo.GetComponentsInChildren<ScriptBala>().Length;
        ContadorText.text = "POINTS: " + contador;
    }

    public void Inicio()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);
    }
}
