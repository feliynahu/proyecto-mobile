using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour
{
    [SerializeField] float drag;
    void Start()
    {
        //GetComponent<Rigidbody>().velocity = new Vector3(0, drag, 0);
        GetComponent<Rigidbody>().angularDrag = drag;
    }

    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso Muerte")) RampasGameManager.GameOver = true;
    }
}
