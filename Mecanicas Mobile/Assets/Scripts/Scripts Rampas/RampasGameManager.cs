using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RampasGameManager : MonoBehaviour
{
    public static bool GameOver;
    [SerializeField] GameObject gameover;
    void Start()
    {
        GameOver = false;
        
    }

    void Update()
    {
        if (GameOver)
        {
            gameover.SetActive(true);
        }
        Inicio();
    }

    public void ReiniciarRampas()
    {
        SceneManager.LoadScene(2);
    }
    public void Inicio()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);
    }
}
