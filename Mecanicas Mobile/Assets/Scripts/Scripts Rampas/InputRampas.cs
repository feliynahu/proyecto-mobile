using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputRampas : MonoBehaviour
{
    [SerializeField] int velocidadRampas;
    [SerializeField]float time;
    private void Start()
    {
       

    }
    void Update()
    {
        RotarRampas();
        time = Time.time;
    }

    private void RotarRampas()
    {
        if (time > 1f) {
            if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
            {
                transform.Rotate(new Vector3(0, 0, -velocidadRampas) * Time.deltaTime);
                ; }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(new Vector3(0, 0, velocidadRampas) * Time.deltaTime);
            }
        }
    }
}
