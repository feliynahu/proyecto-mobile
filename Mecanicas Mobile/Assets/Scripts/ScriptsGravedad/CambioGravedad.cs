using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioGravedad : MonoBehaviour
{
    [SerializeField] Material[] material;
    [SerializeField] float fuerzaNegativa;
    [SerializeField] float fuerzaPositiva;
    Rigidbody rb;
    bool azul;
    void Start()
    {
        azul = false;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Inicio();
        if (azul)
        {
            GetComponent<Renderer>().material = material[0];
            rb.useGravity = true;
            rb.AddForce(new Vector3(0, -fuerzaNegativa, 0), ForceMode.Acceleration);
        }
        else if (!azul)
        {
            GetComponent<Renderer>().material = material[1];
            rb.useGravity = false;
            rb.AddForce(new Vector3(0, fuerzaPositiva, 0), ForceMode.Acceleration);
        }
    }

    private void OnMouseDown()
    {
        if (azul)
        {
            azul = false;
        }
        else if (!azul)
        {
            azul = true;
        }
    }
    public void Inicio()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(3);
    }

}
