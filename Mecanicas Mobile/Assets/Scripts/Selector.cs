using UnityEngine;
using UnityEngine.SceneManagement;

public class Selector : MonoBehaviour
{

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene("Selector");
        }
    }

    public void ColorSwitch()
    {
        SceneManager.LoadScene("Color Switch");
    }

    public void JuegoMundo()
    {
        SceneManager.LoadScene("Juego mundo");
    }

    public void JuegoRampas()
    {
        SceneManager.LoadScene("Juego Rampas");
    }

    public void JuegoGravedad()
    {
        SceneManager.LoadScene("JuegoGravedad");
    }

    public void FlappyBird()
    {
        SceneManager.LoadScene("Flappy Bird");
    }

    public void Salir()
    {
        Debug.Log("Saliste del juego :)");
        Application.Quit();
    }

}

